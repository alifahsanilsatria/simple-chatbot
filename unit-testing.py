import unittest

from app import db,app, socketio, send

class TestSocketIO(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def setUp(self):
        pass

    def test_connect(self):
        client = socketio.test_client(app)
        self.assertTrue(client.is_connected())

    def test_handle_question(self):
        client = socketio.test_client(app)
        client.get_received()

        client.send('how are you?')
        received = client.get_received()
        self.assertEqual(len(received), 1)
        self.assertEqual(received[0]['args'], "i'm fine")

        client.send("thank you")
        received = client.get_received()
        self.assertEqual(len(received), 1)
        self.assertEqual(received[0]['args'], "you are welcome")

        client.send("what's your hobby?")
        received = client.get_received()
        self.assertEqual(len(received), 1)
        self.assertEqual(received[0]['args'], "my hobby is playing games")

if __name__ == '__main__':
    unittest.main()