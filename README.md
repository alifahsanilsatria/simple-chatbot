# simple-chatbot

example how to build image for app.py service using app.Dockerfile:<br>
docker build -f app.Dockerfile -t alifahsanil/simple-chatbot .<br>

example how to run docker image for app.py service:<br>
docker run -p 8888:5000 alifahsanil/simple-chatbot<br>

example how to build image for api-compare-question.py service using api.Dockerfile:<br>
docker build -f api.Dockerfile -t alifahsanil/simple-chatbot-api .<br>

example how to run docker image for api-compare-question.py service:<br>
docker run -p 8889:5001 alifahsanil/simple-chatbot-api<br>

how to run app.py and api-compare-question.py without docker:<br>
python app.py<br>
python api-compare-question.py<br>

access app.py service:<br>
0.0.0.0:8888<br>

Note:<br>
docker image for app.py service failed to run (shows nothing) and for api-compare-question.py service succeed to run
