from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO, send
import requests, json, os

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

socketio = SocketIO(app, cors_allowed_origins="*", ping_timeout=20)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./simple-chatbot.db'
db = SQLAlchemy(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('message')
def handleQuestion(input_question):
    all_question = QuestionAnswerPair.query.all()
    answer = None
    for q in all_question:
        found_question = q.question
        payload = {'input_question': input_question,
                   'found_question': found_question}
        result = requests.post('http://0.0.0.0:5001/question', json=payload)
        result = result.content.decode('utf-8')
        if result == 'true':
            answer = q.answer
            break
    send(answer, broadcast=True)

class QuestionAnswerPair(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(80), unique=True, nullable=False)
    answer = db.Column(db.String(80), unique=True, nullable=False)

    def __repr__(self):
        return '<QuestionAnswerPair id=%r>' % self.id

if __name__ == '__main__':
    socketio.run(app, debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))