import json, os
from flask import Flask, request

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'

@app.route('/question', methods=['POST'])
def compareQuestions():
    payload = request.get_json(force=True)
    input_question = payload['input_question']
    found_question = payload['found_question']
    if input_question == found_question:
        return json.dumps(True)
    else:
        return json.dumps(False)

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 5001)))